$(document).ready(function(){
    $("#search").on('click', function(){
        let search = $.trim( $("input").val() );
         window.location = '/search/'+search;
    });
    $("#input").on('keypress', function(e){
        if(e.which === 13){
            let search = $.trim( $("input").val() );
        window.location = '/search/'+search;
        }
    });
});