const isProd = true;
const option = {
    client: 'mysql',
    connection: {
      host : isProd?'database-1.ctqdrf2rp9xv.ap-southeast-1.rds.amazonaws.com':'127.0.0.1',
      user : isProd?'creator':'root',
      password : isProd?'nightmare':'',
      database : 'archive_movie'
    }
};

module.exports = {
  option
};
